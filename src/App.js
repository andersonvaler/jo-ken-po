import "./App.css";
import { useState, useEffect } from "react";
import UserOptions from "./components/UserOptions";
import Placar from "./components/Placar";
import Moves from "./components/Moves";
import GameOver from './components/GameOver'

const App = () => {
  const [gameOver, setGameOver] = useState(false);
  const [moveNumber, setMoveNumber] = useState(0);
  const [played, setPlayed] = useState(false);
  const [userMove, setUserMove] = useState("");
  const [computerMove, setComputerMove] = useState("");
  const [score, setScore] = useState({ user: 0, computer: 0, draw: 0 });

  const getComputerMove = () => {
    const random = Math.floor(Math.random() * (4 - 1)) + 1;
    let computerMove = "";
    if (random === 1) {
      computerMove = "Pedra";
    }
    if (random === 2) {
      computerMove = "Papel";
    }
    if (random === 3) {
      computerMove = "Tesoura";
    }
    setComputerMove(computerMove);
  };

  const newUserMove = (choice) => {
    setPlayed(true);
    getComputerMove();
    setUserMove(choice);
  };

  const getWinner = () => {
    if (computerMove === userMove) {
      setScore({ ...score, draw: score.draw + 1 });
    } else {
      if (
        (userMove === "Pedra" && computerMove === "Tesoura") ||
        (userMove === "Papel" && computerMove === "Pedra") ||
        (userMove === "Tesoura" && computerMove === "Papel")
      ) {
        setScore({ ...score, user: score.user + 1 });
      } else {
        setScore({ ...score, computer: score.computer + 1 });
      }
    }
    setMoveNumber(moveNumber + 1);
    if (moveNumber === 9) {
      setGameOver(!gameOver);
    }
    setPlayed(false);
  };

  const newGame = () =>{
    setGameOver(false)
    setMoveNumber(0)
    setScore({ user: 0, computer: 0, draw: 0 })
    setComputerMove('')
    setUserMove('')
  }

  useEffect(() => {
    if (played) {
      getWinner();
    }
  });

  return (
    <div className="App">
      <header className="App-header">
        {/* {gameOver && } */}
        <Placar score={score} gameOver={gameOver}></Placar>
        {gameOver ? (
          <GameOver newGame={newGame}></GameOver>
        ) : (
          <Moves computerMove={computerMove} userMove={userMove}></Moves>
        )}
        {!gameOver && <UserOptions choice={newUserMove}></UserOptions>}
      </header>
    </div>
  );
};

export default App;
