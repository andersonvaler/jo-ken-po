const GamerOver = (props) => {
  return (
    <div className="game-over">
      <h2>Gamer Over</h2>
      <button onClick={() => props.newGame()}>Jogar Novamente</button>
    </div>
  );
};

export default GamerOver;
