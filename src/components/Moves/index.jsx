import "./style.css";

const Moves = (props) => {
  return (
    <div className="moves">
      <section>{props.userMove}</section>
      <div className="userP">
        <p>User</p>
      </div>
      <hr></hr>
      <div className="computerP">
        <p>Computer</p>
      </div>
      <section>{props.computerMove}</section>
    </div>
  );
};

export default Moves;
