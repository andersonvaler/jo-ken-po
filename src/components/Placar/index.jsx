import "./style.css";

const Placar = ({ score, gameOver }) => {
  const { user, computer, draw } = score;
  return (
    <div className="score-container">{console.log(gameOver)}
      {gameOver ?  <h3 style={{color:'red'}}>Results</h3>:<h3>Score</h3> }
      <div className="score">
        <div className="user">
          <p className="score-number">{user}</p>
          <p className="player-name">User</p>
        </div>
        <div className="draw">
          <p className="score-number">{draw}</p>
          <p className="player-name">Draw</p>
        </div>
        <div className="computer">
          <p className="score-number">{computer}</p>
          <p className="player-name">Computer</p>
        </div>
      </div>
    </div>
  );
};

export default Placar;
