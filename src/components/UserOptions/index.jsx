import './style.css'

const UserOptions = (props) => {
  return (
    <div className="user-options">
      <button onClick={() => props.choice("Pedra")}>Pedra</button>
      <button onClick={() => props.choice("Papel")}>Papel</button>
      <button onClick={() => props.choice("Tesoura")}>Tesoura</button>
    </div>
  );
};

export default UserOptions;
